package area

import "math"

const Pi = 3.1416

//Circ é responsável por calcular a área da circunferência.
func Circ(raio float64) float64{
    return math.Pow(raio, 2) * Pi
}

//React é responsável por calcular a área de um retângulo.
func Rect(base, altura float64) float64{
    return base * altura
}

// Não é visível
func _TrianguloEq(base, altura float64) float64{
    return (base * altura) / 2
}
